package application;




import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

import controller.AdminScreenController;
import controller.LoginScreenController;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.Photo;
import model.Tag;
import model.User;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

/**
 * The main class of the PhotoAlbum program. 
 * Launches the loginscreen window, and handles loading/saving user data via
 * saveData() and loadUsers().
 */
public class PhotoAlbum extends Application {
	
	private static File saveFile;
	public static ArrayList<User> users;
	public static User currentuser;
	
	private boolean sampleData;
	
	/**
	 * Launches PhotoAlbum application, loads in any prior data and launches the login screen window.
	 * @param primaryStage 
	 */
	@Override
	public void start(Stage primaryStage) throws IOException, ClassNotFoundException {
		
		//////
		isThereSampleData();
		saveFile = new File("data/testData.txt");
		//sampleData=false; //Clear userData.txt and uncomment this to simulate a cold start.
		loadUsers();
		
		
		
			FXMLLoader loader = new FXMLLoader();
			
			loader.setLocation(getClass().getResource("/view/LoginScreen.fxml"));
			AnchorPane root = (AnchorPane)loader.load();
			Scene scene = new Scene(root);
			
			LoginScreenController controller = loader.getController();
			controller.start(primaryStage);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Photo Album");
			primaryStage.setResizable(false);
			
			///////
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
	            public void handle(WindowEvent we) {
	               PhotoAlbum.saveData();
	            }
	        });
			//////////
			
			primaryStage.show();
			
		
	}
	
	/**
	 * Main method, calls launch(args)
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
	
	/**
	 * A method that checks if there is any sample data in the "data" folder. Harcoded to be inside of "testData.txt".
	 * @return Void, but sets boolean sampleData variable to true if any data is found.
	 */
	private void isThereSampleData(){


		File testData = new File("data/testData.txt");

		if (testData.exists()){
			sampleData = true;
		}
		else {
			sampleData = false;
		}

	}
	
	/**
	 * Loads in sample data.
	 * @throws IOException
	 */
	private void loadSampleData() throws IOException{
		
		PhotoAlbum.users = new ArrayList<User>();
		PhotoAlbum.users.add(new User("Milan", "123"));
		PhotoAlbum.users.add(new User("Ethan", "password"));
		
		PhotoAlbum.users.get(0).setAlbum("Pugs");
		PhotoAlbum.users.get(0).setAlbum("Album2");
		
		PhotoAlbum.users.get(1).setAlbum("Funny");
		PhotoAlbum.users.get(1).setAlbum("Cool");
		
		File file = new File("data/photos/pizzapug.jpg");
		Photo test = new Photo(file);
		test.setCaption("cool pug");
		test.addTag(new Tag("Location", "Earth"));
		PhotoAlbum.users.get(0).getAlbum(0).addPhoto(test);
		
		File file2 = new File("data/photos/thinking.jpg");
		Photo test2 = new Photo(file2);
		test2.setCaption("thinkingpug");
		test2.addTag(new Tag("Location", "Earth"));
		test2.addTag(new Tag("Name", "thinkingpug"));
		PhotoAlbum.users.get(0).getAlbum(0).addPhoto(test2);
		
		File file3 = new File("data/photos/evilkermit.jpg");
		Photo test3 = new Photo(file3);
		test3.setCaption("me talking to myself");
		test3.addTag(new Tag("Location", "Earth"));
		test3.addTag(new Tag("Name", "kermit"));
		PhotoAlbum.users.get(1).getAlbum(0).addPhoto(test3);
		
		File file4 = new File("data/photos/spaceparts.jpg");
		Photo test4 = new Photo(file4);
		test4.setCaption("submerged nasa parts");
		test4.addTag(new Tag("Location", "Earth"));
		test4.addTag(new Tag("Location", "Pacific Ocean"));
		PhotoAlbum.users.get(1).getAlbum(1).addPhoto(test4);
		
	}
	
	
	/**
	 * Loads user data from a previous session, unless user data is empty, in which case it loads in any available sample data.
	 * If there is no sample data either, it starts from scratch.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void loadUsers() throws IOException, ClassNotFoundException{
		
			saveFile = new File("src/userData.txt");
			BufferedReader br = new BufferedReader(new FileReader("src/userData.txt"));
			String line = br.readLine();
			if (line == null && sampleData) {
				System.out.println("line is null and test exists");
				 	/*FileInputStream fis = new FileInputStream("data/testData.txt");
				    ObjectInputStream ois = new ObjectInputStream(fis);
				    users = (ArrayList<User>)ois.readObject();
				    //System.out.println(users.size());
				    ois.close();*/
				    loadSampleData();
			}
			else if (line != null && sampleData){
				//userdata is NOT null and sample exists
				FileInputStream fis = new FileInputStream("src/userData.txt");
			    ObjectInputStream ois = new ObjectInputStream(fis);
			    users = (ArrayList<User>)ois.readObject();
			    ois.close();
			}
			else if (line != null && !sampleData){
				//userdata is NOT null and sample DOESNT exists
				FileInputStream fis = new FileInputStream("src/userData.txt");
			    ObjectInputStream ois = new ObjectInputStream(fis);
			    users = (ArrayList<User>)ois.readObject();
			    ois.close();
			}
			else {
				//userdata is null and sample DOESNT exists
				//cold start
				users = new ArrayList<User>();
			}
	}
	
	/**
	 * Serialization, writes user objects to specified file, saveFile.
	 */
	public static void saveData(){
		ArrayList<User> temp = new ArrayList<User>();

		temp.addAll(users);
		try {
		    FileOutputStream fos = new FileOutputStream(saveFile);
		    ObjectOutputStream oos = new ObjectOutputStream(fos);
		    oos.writeObject(temp);
		    oos.flush();
		    oos.close();
		} catch(Exception e) {
		    e.printStackTrace();
		}
	}
	
	
	
	
}
