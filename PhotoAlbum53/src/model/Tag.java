package model;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Tag object class. A tag is simply a "type" string (either "Location", "Name", or "Other") and a value string.
 * @author Ethan Berroa
 * @author Milan Patel
 *
 */

public class Tag implements java.io.Serializable
{
	private String type;
	private String value;
	//public ArrayList<Photo> photos;
	
	/**
	 * Empty constructor.
	 */
	public Tag(){

	}

	/**
	 * Tag constructor that takes in type and value parameters.
	 * @param type
	 * @param value
	 */
	public Tag(String type, String value){
		this.type = type;
		this.value = value;
	}

	/**
	 * Returns type string.
	 * @return
	 */
	public String getType(){
		return type;
	}

	/**
	 * Returns value string.
	 * @return
	 */
	public String getValue(){
		return value;
	}

	/**
	 * Serialization, writes tag data.
	 * @param stream
	 * @throws IOException
	 */
	private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException
	{
        stream.writeObject(type);
        stream.writeObject(value);
    }

	/**
	 * Serialization, reads in tag data.
	 * @param stream
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException
    {
		type = (String) stream.readObject();
        value = (String)stream.readObject();
    }
	
	/**
	 * Overriden equality method, two tags are "equal" if they have the same type and value strings.
	 */
	@Override
	public boolean equals(Object o){
		if (!(o instanceof Tag)) return false;
		else{
			Tag tag = (Tag)o;
			if (tag.getType().equals(this.getType()) && (tag.getValue().equals((this.getValue())))){
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	/**
	 * toString method, returns tag string in the form "Type: Value".
	 */
	public String toString(){
		return type + ":  " + value;
	}

}
