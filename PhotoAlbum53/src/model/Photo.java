package model;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.imageio.ImageIO;

/**
 * Photo object class. A photo contains a date, an image file, a caption, tags, and a pathname.
 * @author Ethan Berroa
 * @author Milan Patel
 *
 */
public class Photo implements java.io.Serializable{

	private Date date;
	private BufferedImage img;
	private Calendar calendar;
	private String URI;
	private String caption;
	public ArrayList<Tag> tags;
	
	/**
	 * Empty constructor.
	 */
	public Photo()
	{
		
	}
	
	/**
	 * Photo constructor with File parameter, initializes date, pathname, and tags.
	 * @param file
	 * @throws IOException
	 */
	public Photo(File file) throws IOException{
				img = ImageIO.read(file);
				if (img != null){
					this.URI = file.toURI().toString();
					//this.tags = new ArrayList<Tag>();
					//this.caption = "";
					long l = file.lastModified();
					//System.out.println(l);
					this.date = new Date(l);
					//System.out.println(date);
					calendar = Calendar.getInstance();
					calendar.setTime(date);
					calendar.set(Calendar.MILLISECOND, 0);
					this.date = calendar.getTime();
					
					this.tags = new ArrayList<Tag>();
					//System.out.println(calendar.getTime());
				}
		
	}

	/**
	 * Returns pathname string.
	 * @return
	 */
	public String getURI() {
		// TODO Auto-generated method stub
		return this.URI;
	}

	/**
	 * Returns Date field.
	 * @return
	 */
	public Date getDate() {
		// TODO Auto-generated method stub
		return this.date;
	}

	/**
	 * Sets the caption field for this photo.
	 * @param caption
	 */
	public void setCaption(String caption){
		
		this.caption = caption;
	}
	
	/**
	 * Overridden equality method, photos are "equal" if they both have the same pathname, meaning they are the exact same file.
	 */
	public boolean equals (Object o)
	{
		if (!(o instanceof Photo)) return false;
		else{
			Photo photo = (Photo)o;
			if (photo.getURI().equals(this.getURI())){
				return true;
			}
			else
			{
				return false;
			}
		}
		
	}
	
	/**
	 * Checks to see if this photo contains the given tag.
	 * @param tmp	tag to check for
	 * @return
	 */
	public boolean containsTag(Tag tmp)
	{
		for(int i = 0; i < tags.size(); i++)
		{
			if(tmp.equals(tags.get(i)))
				return true;
		}
		return false;
	}
	
	/**
	 * Adds parameter to this photo's tags.
	 * @param tmp
	 * @return
	 */
	public boolean addTag(Tag tmp)
	{
		if(!this.containsTag(tmp))
		{
			tags.add(tmp);
			return true;
		}
		return false;
	}
	
	/**
	 * Returns caption string.
	 * @return
	 */
	public String getCaption() {
		// TODO Auto-generated method stub
		return this.caption;
	}
	
	/**
	 * Serialization, writes photo data.
	 * @param stream
	 * @throws IOException
	 */
	private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException
	{
        stream.writeObject(URI);
        stream.writeObject(date);
        stream.writeObject(caption);
        stream.writeObject(tags);

    }
	
	/**
	 * Sets pathname string.
	 * @param URI
	 */
	public void setURI(String URI){
		this.URI = URI;
	}

	/**
	 * Serialization, reads photo data.
	 * @param stream
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException
    {
		URI = (String) stream.readObject();
        date = (Date)stream.readObject();
        caption = (String)stream.readObject();
        tags = (ArrayList<Tag>)stream.readObject();
    }
	
	
}
