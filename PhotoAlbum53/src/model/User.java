package model;

import java.io.IOException;
import java.util.ArrayList;

/**
 * User object class. A user is comprised of a name, a password (useless), a currentAlbumIndex (keeps track of which album is open),
 * and an arrayList of all albums this user has. 
 * @author Ethan Berroa
 * @author Milan Patel
 *
 */
public class User implements java.io.Serializable{

	public String name;
	public String password;
	public int currentAlbumIndex;
	public ArrayList<Album> albums;
	
	/**
	 * Constructor, initializes name, password, and album fields.
	 * @param name
	 * @param password
	 */
	public User(String name, String password)
	{
		this.name=name;
		this.password = password;
		albums = new ArrayList<Album>();
	}
	
	/**
	 * Serialization, writes user data.
	 * @param stream
	 * @throws IOException
	 */
	private void writeObject(java.io.ObjectOutputStream stream)
            throws IOException
	{
        stream.writeObject(name);
        stream.writeObject(password);
        stream.writeObject(albums);
    }

	/** 
	 * Serialization, reads in user data.
	 * @param stream
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException
    {
        name = (String) stream.readObject();
        password = (String) stream.readObject();
        albums = (ArrayList<Album>) stream.readObject();
    }
	
	
	/**
	 * Adds a new album to this user's list.
	 * @param name
	 */
	public void setAlbum(String name)
	{
		albums.add(new Album(name));
	}
	
	/**
	 * Changes the current album open.
	 * @param i
	 */
	public void setCurrentAlbum(int i)
	{
		this.currentAlbumIndex = i;
	}
	
	/**
	 * ToString, just returns name string.
	 */
	public String toString()
	{
		return name;
	}

	/**
	 * Gets the specified album from the album list via index i.
	 * @param i
	 * @return
	 */
	public Album getAlbum(int i){
		return this.albums.get(i);
	}
	
	/**
	 * Gets the specified album from this users' list that has the specified NAME, returns null if there is no such album.
	 * @param name
	 * @return
	 */
	public Album getAlbum(String name){
		for(int i = 0; i < this.albums.size(); i++)
		{
			if(this.albums.get(i).name.equals(name))
				return this.albums.get(i);
		}
		return null;
	}
	
	/**
	 * Returns the entire album list.
	 * @return
	 */
	public ArrayList<Album> getAlbums() {
		// TODO Auto-generated method stub
		return this.albums;
	}

	/**
	 * Deletes the album from the list at index i.
	 * @param i
	 */
	public void deleteAlbum(int i) {
		this.albums.remove(i);
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Overriden equality method, two Users are equal if they have the same name fields.
	 */
	public boolean equals (Object o)
	{
		if (!(o instanceof User)) return false;
		else{
			User user = (User)o;
			if (user.name.equals(this.name)){
				return true;
			}
			else
			{
				return false;
			}
		}
		
	}
	
	
}
