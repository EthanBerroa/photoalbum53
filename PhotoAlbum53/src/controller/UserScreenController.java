package controller;

import javafx.event.ActionEvent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import application.PhotoAlbum;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Album;
import model.User;


/**
 * Controller for the userScreen, the screen that displays all the users albums. Albums contain a name label, a size label, and a
 * date range label.
 * @author Ethan
 *
 */
public class UserScreenController {

	@FXML
	private TilePane albumpane;
	
	private User user;

	private Stage primaryStage;
	private VBox selectedTile;
	
	//public Album currentAlbum;
	
	/**
	 * Initializes tilePane with user albums.
	 */
	@FXML
	private void initialize(){
		user = PhotoAlbum.currentuser;
		ArrayList<Album> albums = user.getAlbums();
		//if (albums == null){
		//	System.out.println("albums is null");
		//}
	    for (int i = 0; i < albums.size(); i++) {
           VBox vbox = createVBox(albums.get(i));
	       albumpane.getChildren().add(vbox);
	    }
	    albumpane.setStyle("-fx-background-color: white;");
	}
	
	/**
	 * Logs the user out, taking them back to the loginScreen. Saves data.
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void handleLogout(ActionEvent event) throws IOException
	{
		PhotoAlbum.saveData();
		FXMLLoader loader = new FXMLLoader();
	    loader.setLocation(getClass().getResource("/view/LoginScreen.fxml"));
		AnchorPane root = (AnchorPane)loader.load();
		Scene scene = new Scene(root);
		LoginScreenController controller = loader.getController();
		controller.start(primaryStage);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	
	/**
	 * Handles a search request when the user clicks File->Search. Launches a new searchView window.
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void handleSearch(ActionEvent event) throws IOException 
	{
		//probably just load search screen 
		
		
		Stage stage = new Stage();
		FXMLLoader loader = new FXMLLoader();
	    loader.setLocation(getClass().getResource("/view/SearchView.fxml"));
		AnchorPane root = (AnchorPane)loader.load();
		Scene scene = new Scene(root);
		SearchController controller = loader.getController();
		controller.start(stage);
		stage.setScene(scene);
		stage.setResizable(false);
		stage.setTitle("Photo Search");
		stage.show();
		this.primaryStage.close();
	}
	
	
	/**
	 * Allows user to create a new album, but does not allow for two albums to have the same name.
	 * @param event
	 */
	@FXML
	private void handleCreate(ActionEvent event)
	{
		
			 final Stage dialog = new Stage();
            dialog.initModality(Modality.APPLICATION_MODAL);
            dialog.initOwner(primaryStage);
            VBox dialogVbox = new VBox(20);
              
              TextField name = new TextField("Album Name");
              
              Button ok = new Button("CONFIRM");
              Button cancel = new Button("CANCEL");

              dialogVbox.getChildren().addAll(name, ok, cancel);
              
              dialogVbox.setPadding(new Insets(10, 50, 50, 50));
              
              /////If User clicks on OK we go here////////////////////
              ok.setOnAction(new EventHandler<ActionEvent>() {
           	   
      	        @Override
      	    	public void handle(ActionEvent e){		    	       	        		
      	        	
      	        	
      	        	if(!name.getText().equals(""))
      	        	{
      	        		if(!user.getAlbums().contains(new Album(name.getText())))
      	        		{
      	        			
      	        			user.setAlbum(name.getText());
          	        		VBox vbox = createVBox(user.getAlbum(name.getText()));
          	        		albumpane.getChildren().add(vbox);
      	        		}
      	        		else
      	        		{
      	        			//System.out.println("testing");
      	        			Alert alert = new Alert(AlertType.ERROR);
   	    		    	 alert.setTitle("Duplicate Album name");
   	    		    	 alert.setHeaderText("Name cannot be the same as existing albums.");
   	    		    	 alert.setContentText("Please click to continue");
   	    		    	 alert.showAndWait();
      	        		}
      	        	}
   				   dialog.close();
      	        	
      	        }});
              
              /////If User hits CANCEL button, just exit window///////
              cancel.setOnAction(new EventHandler<ActionEvent>() {
	       	        @Override
	       	    	public void handle(ActionEvent e){
	    				   dialog.close();
	       	        	
	       	        }});
              
              Scene dialogScene = new Scene(dialogVbox, 300, 150);
              dialog.setScene(dialogScene);
              dialog.showAndWait();
		
	}
	
	/**
	 * Closes the program, saves data.
	 * @param event
	 */
	@FXML
	private void handleExit(ActionEvent event)
	{
		PhotoAlbum.saveData();
		primaryStage.close();
	}
	
	/**
	 * Allows user to rename a selected album.
	 * @param event
	 */
	@FXML
	private void handleRename(ActionEvent event)
	{
		ArrayList<Album> albums = user.getAlbums();
		
		if (selectedTile != null){
			
			final String title = ((Label) selectedTile.getChildren().get(1)).getText();
	
			Stage dialog = new Stage();
            dialog.initModality(Modality.APPLICATION_MODAL);
            dialog.initOwner(primaryStage);
            VBox dialogVbox = new VBox(20);
              
            TextField name = new TextField(title.substring(7));
              
            Button ok = new Button("CONFIRM");
            Button cancel = new Button("CANCEL");

              dialogVbox.getChildren().addAll(name, ok, cancel);
              dialogVbox.setPadding(new Insets(10, 50, 50, 50));
              
              /////If User clicks on OK we go here////////////////////
              ok.setOnAction(new EventHandler<ActionEvent>() {
           	   
      	        @Override
      	    	public void handle(ActionEvent e){		    	       	        		
      	        	if(!user.getAlbums().contains(new Album(name.getText())))
      	        	{
      	        		user.getAlbum(title.substring(7)).name=name.getText();
      	        	}
      	        	else
      	        	{
      	        		Alert alert = new Alert(AlertType.ERROR);
  	    		    	 alert.setTitle("Duplicate Album name");
  	    		    	 alert.setHeaderText("Name cannot be the same as existing albums.");
  	    		    	 alert.setContentText("Please click to continue");
  	    		    	 alert.showAndWait();
      	        	}
   				   dialog.close();
      	        	
      	        }});
              
              /////If User hits CANCEL button, just exit window///////
              cancel.setOnAction(new EventHandler<ActionEvent>() {
	       	        @Override
	       	    	public void handle(ActionEvent e){
	    				   dialog.close();
	       	        	
	       	        }});
              
              Scene dialogScene = new Scene(dialogVbox, 300, 150);
              dialog.setScene(dialogScene);
              dialog.showAndWait();
            
			albumpane.getChildren().clear();
		    for (int i = 0; i < albums.size(); i++) {
	           VBox vbox = createVBox(albums.get(i));
		       albumpane.getChildren().add(vbox);
		    }
		}

	    selectedTile = null;
	}
	
	/**
	 * Allows user to delete an album, asks for confirmation first.
	 * @param event
	 */
	@FXML 
	private void handleDelete(ActionEvent event)
	{
		Alert alert = new Alert(AlertType.CONFIRMATION);
	   	 alert.setTitle("Delete Confirm");
	   	 alert.setHeaderText("Would you like to delete this album?");
	   	 alert.setContentText("Please click to confirm.");

	   	 Optional<ButtonType> result = alert.showAndWait();
	   	 
	   	 ////If OK then delete the song and re-sort list/////
	   	 if (result.get() == ButtonType.OK){
	   		
	   		ArrayList<Album> albums = user.getAlbums();
			if (selectedTile != null){
				String title = ((Label) selectedTile.getChildren().get(1)).getText();
				title = title.substring(7);
				for (int i = 0; i < albums.size(); i++){
					if (title.equals(albums.get(i).getAlbumName())){
						user.deleteAlbum(i);
					}
				}
			}
			albumpane.getChildren().clear();
		    for (int i = 0; i < albums.size(); i++) {
	           VBox vbox = createVBox(albums.get(i));
		       albumpane.getChildren().add(vbox);
		    }
		    selectedTile = null;
				
	   		 
	   	 } else {
	   	     /////If then click cancel, don't do anything just close////
	   	 }
	}
	
	/**
	 * Start method, initializes the primaryStage field.
	 * @param primaryStage
	 */
	public void start(Stage primaryStage){
		this.primaryStage = primaryStage;
	}


	
	/**
	 * Creates vBoxes to populate the windows tilePane, using a hardcoded folder thumbnail image, a name label, a 
	 * size label, and a date range label.
	 * @param album
	 * @return
	 */
	public VBox createVBox(Album album){
		ImageView imageView = new ImageView(new File ("src/folder.png").toURI().toString());
	    Label title = new Label("Title: " + album.getAlbumName());
	    imageView.setFitHeight(100);
	    imageView.setFitWidth(100);
	    Label date = new Label("Dates: " + album.dateRange());
	    Label numPhotos = new Label("Size: " + album.getPhotos().size());
	    VBox vbox = new VBox(imageView, title, date, numPhotos);

	    //mouse click (not double click)
	    vbox.setOnMousePressed(new EventHandler<MouseEvent>(){

			@Override
	    	public void handle(MouseEvent e){
	    		ObservableList<Node> nodes = albumpane.getChildren();
	    		for (int i = 0; i < nodes.size(); i++){
	    			nodes.get(i).setStyle("-fx-background-color: white;");
	    		}
	    		vbox.setStyle("-fx-background-color: #ccffff;");
	    		selectedTile = vbox;
	    	}
	    });
	    
	    vbox.setOnMouseClicked(new EventHandler<MouseEvent>() {
	        @Override
	        public void handle(MouseEvent m) {
	            if(m.getButton().equals(MouseButton.PRIMARY)){
	                if(m.getClickCount() == 2){
	                	try{
	                		ArrayList<Album> albums = user.getAlbums();
	            			if (selectedTile != null){
	            				String title = ((Label) selectedTile.getChildren().get(1)).getText();
	            				title = title.substring(7);
	            				for (int i = 0; i < albums.size(); i++){
	            					if (title.equals(albums.get(i).getAlbumName())){
	            						user.setCurrentAlbum(i);
	            						break;
	            					}
	            				}
	            			}

	                		FXMLLoader loader = new FXMLLoader();
		        		    loader.setLocation(getClass().getResource("/view/AlbumView.fxml"));
		        			AnchorPane root = (AnchorPane)loader.load();
		        			Scene scene = new Scene(root);
		        			AlbumViewController controller = loader.getController();
		        			controller.start(primaryStage);
		        			
		        			primaryStage.setScene(scene);
		        			primaryStage.show();

	                	}catch (Exception e){
	                		e.printStackTrace();
	                	}

	                }
	            }
	        }
	    });
	    
	    return vbox;
	}
	

}
