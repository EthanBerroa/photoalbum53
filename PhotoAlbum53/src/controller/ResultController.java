package controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.imageio.ImageIO;

import application.PhotoAlbum;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.Album;
import model.Photo;
import model.User;

/**
 * Controller to handle the RESULTS screen, basically the screen that occurs after a search request, displaying the results
 * similar to how albumView displays photo thumbnails. User can either close window outright or create a new album with the search 
 * results.
 * @author Ethan Berroa
 * @author Milan Patel
 *
 */
public class ResultController {

	public static Photo photoEdit;
	public static Album tmpalbum;
	private User user;
	@FXML
	private TilePane photopane;
	private Stage primaryStage;
	
	private VBox selectedTile;
	
	/**
	 * Initializes tilePane to contain the thumbnails of the searched photos.
	 */
	@FXML
	private void initialize(){
		user = SearchController.user;
		tmpalbum = new Album("tmp"); 
		
		tmpalbum.photos = SearchController.photos;
		
		AlbumViewController.album = tmpalbum; //so photoviewer doesnt get confused when checking previous and next photos
		
		ArrayList<Photo> photos = tmpalbum.getPhotos();

		
		
	    for (int i = 0; i < photos.size(); i++) {
           VBox vbox = createVBox(photos.get(i));
	       photopane.getChildren().add(vbox);
	    }
	    photopane.setStyle("-fx-background-color: white;");
	    photopane.setPadding(new Insets(15, 15, 15, 15));
	    photopane.setVgap(10);
        photopane.setHgap(10);
	   
	}

	/**
	 * Start method, initializes primaryStage field.
	 * @param primaryStage
	 */
	public void start(Stage primaryStage){
		this.primaryStage = primaryStage;
	}

	/**
	 * Handles when user clicks "Create New Album" button, creates a new album with the results. Asks user for a name, but does
	 * not allow names that already exist.
	 * @param event
	 */
	@FXML
	private void createNewAlbum(ActionEvent event)
	{
		final Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(primaryStage);
        VBox dialogVbox = new VBox(20);
          
          TextField name = new TextField("Album Name");
          
          Button ok = new Button("CONFIRM");
          Button cancel = new Button("CANCEL");

          dialogVbox.getChildren().addAll(name, ok, cancel);
          
          dialogVbox.setPadding(new Insets(10, 50, 50, 50));
          
          /////If User clicks on OK we go here////////////////////
          ok.setOnAction(new EventHandler<ActionEvent>() {
       	   
  	        @Override
  	    	public void handle(ActionEvent e){		    	       	        		
  	        	
  	        	tmpalbum.name=name.getText();
  	        	if(!user.albums.contains(tmpalbum))
  	        		user.albums.add(tmpalbum);
  	        	else
  	        	{
  	        		Alert alert = new Alert(AlertType.ERROR);
	    		    	 alert.setTitle("Duplicate Album name");
	    		    	 alert.setHeaderText("Name cannot be the same as existing albums.");
	    		    	 alert.setContentText("Please click to continue");
	    		    	 alert.showAndWait();
  	        	}
				dialog.close();
  	        	
  	        }});
          
          /////If User hits CANCEL button, just exit window///////
          cancel.setOnAction(new EventHandler<ActionEvent>() {
       	        @Override
       	    	public void handle(ActionEvent e){
    				   dialog.close();
       	        	
       	        }});
          
          Scene dialogScene = new Scene(dialogVbox, 300, 150);
          dialog.setScene(dialogScene);
          dialog.showAndWait();
	}
	
	/**
	 * Closes search results window, goes back to the userView. Saves data.
	 * @param event
	 * @throws IOException
	 */
	@FXML 
	private void goBack(ActionEvent event) throws IOException
	{
		
		PhotoAlbum.saveData();
		FXMLLoader loader = new FXMLLoader();
	    loader.setLocation(getClass().getResource("/view/UserScreen.fxml"));
		AnchorPane root = (AnchorPane)loader.load();
		Scene scene = new Scene(root);
		UserScreenController controller = loader.getController();
		controller.start(primaryStage);
		primaryStage.setScene(scene);
			
		primaryStage.show();
	
	}
	
	/**
	 * Creates the vBoxes that populate the search results screen using photo thumbnails and captions and dates.
	 * @param photo
	 * @return
	 */
	public VBox createVBox(Photo photo){
		Image image = new Image(photo.getURI());
		ImageView imageView = new ImageView(image);
		imageView.setPreserveRatio(true);
		imageView.setSmooth(true);
		imageView.setFitWidth(135);
		Label uri = new Label(photo.getURI());
		uri.managedProperty().bind(uri.visibleProperty());
		uri.setVisible(false);
	    Label title = new Label("Caption: " + photo.getCaption());
	    Label date = new Label("Date: " + new SimpleDateFormat("MM.dd.YYYY").format(photo.getDate()));
	    
	    VBox vbox = new VBox(imageView, title, date, uri);
	    
	    //date.setAlignment(Pos.BOTTOM_LEFT);

	    vbox.setOnMousePressed(new EventHandler<MouseEvent>(){
	    	

			@Override
	    	public void handle(MouseEvent e){
	    		ObservableList<Node> nodes = photopane.getChildren();
	    		for (int i = 0; i < nodes.size(); i++){
	    			nodes.get(i).setStyle("-fx-background-color: white;");
	    		}
	    		vbox.setStyle("-fx-background-color: #ccffff;");
	    		selectedTile = vbox;
	    	}
	    });

	    //double clicking
	    vbox.setOnMouseClicked(new EventHandler<MouseEvent>() {
	        @Override
	        public void handle(MouseEvent m) {
	            if(m.getButton().equals(MouseButton.PRIMARY)){
	                if(m.getClickCount() == 2){
	                	try{
	                		ArrayList<Photo> photos = tmpalbum.getPhotos();
	            			if (selectedTile != null){
	            				String uri = ((Label) selectedTile.getChildren().get(3)).getText();
	            				for (int i = 0; i < photos.size(); i++){
	            					if (uri.equals(photos.get(i).getURI())){
	            						AlbumViewController.photoEdit = photos.get(i);
	            						break;
	            					}
	            				}
	            			}
	            			Stage stage = new Stage();
	                		FXMLLoader loader = new FXMLLoader();
		        		    loader.setLocation(getClass().getResource("/view/PhotoView.fxml"));
		        			AnchorPane root = (AnchorPane)loader.load();
		        			Scene scene = new Scene(root);
		        			PhotoViewController controller = loader.getController();
		        			controller.start(stage);
		        			stage.setScene(scene);
		        			stage.setResizable(false);
		        			stage.setTitle("Photo Viewer");
		        			stage.showAndWait();
	                	}catch (Exception e){
	                		e.printStackTrace();
	                	}

	                }
	            }
	        }
	    });
	    return vbox;
	}
	
	
	
}
