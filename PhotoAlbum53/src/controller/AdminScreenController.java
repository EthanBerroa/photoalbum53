package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import application.PhotoAlbum;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.User;

/**
 * Controller for the admin screen window, a simple listview with usernames. Handles deleting and adding new users.
 * @author Ethan Berroa
 * @author Milan Patel
 *
 */
public class AdminScreenController {

	@FXML
	private MenuItem logoutButton;
	@FXML
	private MenuItem exit;
	@FXML
	private MenuItem add;
	@FXML
	private MenuItem delete;
	@FXML
	private ListView<User> listView;

	private ObservableList<User> usersObservable;

	//public static ArrayList<User> users;

	private Stage primaryStage;

	/**
	 * Initializes listView contents to photoAlbum users.
	 */
	@FXML
	private void initialize(){
		usersObservable = FXCollections.observableArrayList();
		usersObservable.addAll(PhotoAlbum.users);
		listView.setItems(usersObservable);
        listView.getSelectionModel().select(0);

	}
	
	/**
	 * Closes program from the admin window screen, saves data.
	 * @param event
	 */
	@FXML
	private void handleExit(ActionEvent event){
		try{
			PhotoAlbum.saveData();
			primaryStage.close();
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Logs out admin, goes back to the user login screen, saves data.
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void handleLogout(ActionEvent event) throws IOException{
		
			//save?
			PhotoAlbum.saveData();
			FXMLLoader loader = new FXMLLoader();
		    loader.setLocation(getClass().getResource("/view/LoginScreen.fxml"));
			AnchorPane root = (AnchorPane)loader.load();
			Scene scene = new Scene(root);
			LoginScreenController controller = loader.getController();
			controller.start(primaryStage);
			primaryStage.setScene(scene);
			primaryStage.show();
		
	}
	
	/**
	 * Launches AddUserPopup window stage, which handles the actual adding of a new user. Waits for that to return,
	 * then reorganizes listView.
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void handleAddUser(ActionEvent event) throws IOException{
		
		Stage stage = new Stage();
		FXMLLoader loader = new FXMLLoader();
	    loader.setLocation(getClass().getResource("/view/AddUserPopup.fxml"));
		AnchorPane root = (AnchorPane)loader.load();
		Scene scene = new Scene(root);
		AddUserPopupController controller = loader.getController();
		controller.start(stage);
		stage.setScene(scene);
		stage.setResizable(false);
		stage.setTitle("Add New User");
		stage.showAndWait();
		usersObservable.setAll(PhotoAlbum.users);
		listView.setItems(usersObservable);
		
	}
	
	/**
	 * Handles the deletion of a selected user, asks for confirmation first, then removes user and reorganizes listView.
	 * @param e
	 */
	@FXML
	private void handleDeleteUser(ActionEvent e)
	{
		Alert alert = new Alert(AlertType.CONFIRMATION);
   	 alert.setTitle("Delete Confirm");
   	 alert.setHeaderText("Would you like to delete this user?");
   	 alert.setContentText("Please click to confirm.");

   	 Optional<ButtonType> result = alert.showAndWait();
   	 
   	 ////If OK then delete the song and re-sort list/////
   	 if (result.get() == ButtonType.OK){
   		
   		 int removeIndex = listView.getSelectionModel().getSelectedIndex();
   		 
			 PhotoAlbum.users.remove(removeIndex);
			 usersObservable.setAll(PhotoAlbum.users);
				listView.setItems(usersObservable);
   		 
   	 } else {
   	     /////If then click cancel, don't do anything just close////
   	 }
	}
	
	/**
	 * Start method, sets local primaryStage to given parameter stage.
	 * @param primaryStage
	 */
	public void start(Stage primaryStage){
		this.primaryStage = primaryStage;
	}

	
	
	
}
