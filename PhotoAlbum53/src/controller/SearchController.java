package controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import application.PhotoAlbum;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.Photo;
import model.Tag;
import model.User;

/**
 * Controller for searchView window, the window that pops up when user wants to search for a photo in their albums.
 * Contains fields for tags and dates. A date range can be provided, but if only one date is provided the results will simply
 * look for photos after or before that one date.
 * @author Ethan Berroa
 *
 */
public class SearchController {

	@FXML
	public TextField locationbox;
	
	@FXML
	public TextField namebox;
	
	@FXML
	public TextField otherbox;
	
	@FXML
	public DatePicker startdate;
	
	@FXML
	public DatePicker enddate;
	
	public static User user;
	public static ArrayList<Photo> photos;
	private Stage primarystage;
	private Date start;
	private Date end;
	
	/**
	 * Cancels the search, closes window and returns to userScreen.
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void handleCancel(ActionEvent event) throws IOException
	{
		FXMLLoader loader = new FXMLLoader();
	    loader.setLocation(getClass().getResource("/view/UserScreen.fxml"));
		AnchorPane root = (AnchorPane)loader.load();
		Scene scene = new Scene(root);
		UserScreenController controller = loader.getController();
		controller.start(primarystage);
		primarystage.setScene(scene);
		primarystage.show();
	}
	
	/**
	 * Searches for the results based on the given parameters, opens a resultsView window to display any results.
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void handleSearch(ActionEvent event) throws IOException
	{
		
		String [] locationTags = null;
		String [] nameTags = null;
		String [] otherTags = null;
		System.out.println(locationbox.getText());
		if(locationbox.getText().equals("") == false)
			locationTags = locationbox.getText().split(" ");
		if(namebox.getText().equals("") == false)
			nameTags = namebox.getText().split(" ");
		if(otherbox.getText().equals("") == false)
			otherTags = otherbox.getText().split(" ");
		
		if(startdate.getValue()!=null)
		{
			LocalDate localstart = startdate.getValue();
			start = Date.from(localstart.atStartOfDay(ZoneId.systemDefault()).toInstant());
		}
		
		if(enddate.getValue()!=null)
		{
			LocalDate localend = enddate.getValue();
			end = Date.from(localend.atStartOfDay(ZoneId.systemDefault()).toInstant());
		}
		
		boolean keep = false;
		
		
		
		if(locationTags!=null || nameTags !=null || otherTags != null)
		{
		for(int i=0; i < photos.size(); i++)
		{
			if(locationTags != null)
			{
				
				for(int j = 0; j < locationTags.length; j++)
					if(photos.get(i).containsTag(new Tag("Location", locationTags[j])))
					{
						System.out.println("86");
						keep = true;
						
					}
			}
			
			if(nameTags != null)
			{
				for(int j = 0; j < nameTags.length; j++)
					if(photos.get(i).containsTag(new Tag("Name", nameTags[j])))
						keep=true;
			}
			
			if(otherTags != null)
			{
				for(int j = 0; j < otherTags.length; j++)
					if(photos.get(i).containsTag(new Tag("Other", otherTags[j])))
						keep=true;
			}
			if(keep)
			{
				keep=false;
				continue;
			}
			else
			{
				photos.remove(i);
				i--;
			}
			
		}
		}
		
		if(start!=null || end!=null)
		{
			
			for(int i = 0; i < photos.size(); i++)
			{
				if(start != null && photos.get(i).getDate().before(start))
				{
					photos.remove(i);
					i--;
				}
				else if(end != null && photos.get(i).getDate().after(end))
				{
					photos.remove(i);
					i--;
				}
			}
		}
		
		
		FXMLLoader loader = new FXMLLoader();
	    loader.setLocation(getClass().getResource("/view/ResultView.fxml"));
		AnchorPane root = (AnchorPane)loader.load();
		Scene scene = new Scene(root);
		ResultController controller = loader.getController();
		controller.start(primarystage);
		
		primarystage.setScene(scene);
		
		primarystage.show();
		
	}
	
	/**
	 * Initializes an array of all the users photos to be used for searching.
	 */
	@FXML
	private void initialize()
	{
		
		user = PhotoAlbum.currentuser;
		photos = new ArrayList<Photo>();
		
		for(int i = 0; i < user.getAlbums().size(); i++)
		{
			for(int j = 0; j < user.getAlbums().get(i).getPhotos().size(); j++)
			{
				photos.add(user.getAlbums().get(i).getPhotos().get(j));
			}
		}
	}
	
	/**
	 * Start method, initializes primaryStage field and sets window to call saveData() upon closure.
	 * @param stage
	 */
	public void start(Stage stage){
		this.primarystage = stage;
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
            	try{
        			
        			PhotoAlbum.saveData();
        			stage.close();
        		}catch(Exception e){
        			e.printStackTrace();
        		}
            }
        });
	}

	
	

}
