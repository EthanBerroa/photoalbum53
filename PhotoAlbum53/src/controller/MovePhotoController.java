package controller;

import application.PhotoAlbum;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.Tag;

/**
 * Another tiny controller class meant to handle the movePhoto window, the window that pops up when a user desires to move/copy
 * a photo into another album. Contains a listView to display album names, and two buttons: one for moving photos and one for copying 
 * photos. Also has a tiny image preview to the side, so user knows exactly which photo they are moving/copying.
 * @author Ethan Berroa
 * @author Milan Patel
 *
 */
public class MovePhotoController {

	private Stage primaryStage;
	
	boolean move;
	
	private Photo photocopy;
	
	@FXML
	ImageView imagePreview;
	
	private ObservableList<Album> albumObservable;
	
	@FXML 
	ListView albumList;
	
	/**
	 * Initializes listView to contain album names. Also initiliazes small image preview of selected image.
	 */
	@FXML
	private void initialize(){
		
		photocopy = AlbumViewController.photoEdit;
		move = false;
		imagePreview.setImage(new Image(photocopy.getURI()));
		
		albumObservable = FXCollections.observableArrayList();
		albumObservable.addAll(PhotoAlbum.currentuser.getAlbums());
		albumList.setItems(albumObservable);
        albumList.getSelectionModel().select(0);
		
	}

	/**
	 * Start method, sets local field primaryStage to given parameter.
	 * @param primaryStage
	 */
	public void start(Stage primaryStage){
		this.primaryStage = primaryStage;
	}

	/**
	 * Handles a move request by the user, upon clicking the move button it moves a photo album into the selected album and removes 
	 * said photo from it's original album. Can move one photo to multiple albums but does not allow you to move the same photo to the same album more than once. 
	 * @param e
	 */
	@FXML
	private void handleMove(ActionEvent e)
	{
		int moveIndex = albumList.getSelectionModel().getSelectedIndex();
		if(!(PhotoAlbum.currentuser.getAlbum(moveIndex).getPhotos().contains(photocopy)))
		{
			PhotoAlbum.currentuser.getAlbum(moveIndex).addPhoto(photocopy);
			AlbumViewController.album.getPhotos().remove(photocopy);
		}
				
		
	}
	
	/**
	 * Handles a copy request from the user, upon clicking the copy button. Copies selected photo to selected album, which leaves the
	 * original photo in the original album. Can copy to multiple albums but cannot copy the same photo to the same album more than once.
	 * @param e
	 */
	@FXML
	private void handleCopy(ActionEvent e)
	{
		int moveIndex = albumList.getSelectionModel().getSelectedIndex();
		
		if(!(PhotoAlbum.currentuser.getAlbum(moveIndex).getPhotos().contains(photocopy)))
			PhotoAlbum.currentuser.getAlbum(moveIndex).addPhoto(photocopy);
		
	}
	
	/**
	 * Closes movePhoto window.
	 * @param e
	 */
	@FXML
	private void handleClose(ActionEvent e)
	{
		primaryStage.close();
	}
	
	
	
}
