package controller;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.imageio.ImageIO;

import application.PhotoAlbum;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.User;

/**
 * Displays the contents INSIDE an album: photo thumbnails, with captions and dates.
 * Users can doubleclick photos to proceed to photoView, add/delete photos, edit captions, and edit tags. 
 * Duplicate photos are not allowed, in this implementation "duplicate" means two files cannot have the same path
 * in the file system.
 * @author Ethan Berroa
 * @author Milan Patel
 *
 */
public class AlbumViewController {

	public static Photo photoEdit;
	public static Album album;
	private User user;
	@FXML
	private TilePane photopane;
	private Stage primaryStage;
	
	private VBox selectedTile;
	
	/**
	 * Initializes tilePane to user photo thumbnails. 
	 */
	@FXML
	private void initialize(){
		user = PhotoAlbum.currentuser;
		album = user.getAlbums().get(user.currentAlbumIndex);
		ArrayList<Photo> photos = album.getPhotos();

	    for (int i = 0; i < photos.size(); i++) {
           VBox vbox = createVBox(photos.get(i));
	       photopane.getChildren().add(vbox);
	    }
	    photopane.setStyle("-fx-background-color: white;");
	    photopane.setPadding(new Insets(15, 15, 15, 15));
	    photopane.setVgap(10);
        photopane.setHgap(10);
	   
	}

	/**
	 * Start method, sets local field primaryStage to stage parameter.
	 * @param primaryStage
	 */
	public void start(Stage primaryStage){
		this.primaryStage = primaryStage;
	}


	/**
	 * Handler method for when user clicks BACK button, bringing the user back out of the current album and back to the userScreen
	 * @param event
	 * @throws IOException
	 */
	@FXML 
	private void goBack(ActionEvent event) throws IOException
	{
		PhotoAlbum.saveData();
		FXMLLoader loader = new FXMLLoader();
	    loader.setLocation(getClass().getResource("/view/UserScreen.fxml"));
		AnchorPane root = (AnchorPane)loader.load();
		Scene scene = new Scene(root);
		UserScreenController controller = loader.getController();
		controller.start(primaryStage);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	/**
	 * Handler method for when user selects Add Photo option, launches a fileChooser window that allows user to pick from
	 * their personal file system what photo they'd like to import. Prevents adding duplicate files. 
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void handleAddPhoto(ActionEvent event) throws IOException{
			Stage stage = new Stage();
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Select Image files");
			List<File> pictures = fileChooser.showOpenMultipleDialog(stage);
			if (pictures != null){
				for (int i = 0; i < pictures.size(); i++){

					BufferedImage image = ImageIO.read(pictures.get(i));
					if(image != null)
					{
						Photo photo = new Photo(pictures.get(i));
						if(!album.getPhotos().contains(photo))
						{
							photo.setCaption(pictures.get(i).getName());
							album.addPhoto(photo);
						}
					}
				}
			}
			selectedTile = null;
			photopane.getChildren().clear();
			for (int i = 0; i < album.getPhotos().size(); i++) {
		           VBox vbox = createVBox(album.getPhotos().get(i));
			       photopane.getChildren().add(vbox);
			    }		
	}
	
	/**
	 * Allows the user to edit the caption of any selected photo. Launches a small dialog box where user can enter the new caption,
	 * and also cancel in case they change their mind.
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void editCaption (ActionEvent event) throws IOException
	{
	
		Photo tmp = new Photo();
		
		if (selectedTile != null){
			String title = ((Label) selectedTile.getChildren().get(1)).getText();
			title = title.substring(9);
			
			for (int i = 0; i < album.getPhotos().size(); i++){
				
				if (title.equals(album.getPhotos().get(i).getCaption())){ //change to caption or w/e later
					
					tmp = album.getPhotos().get(i);
					break;
				}
			}
		
		
		final Photo toEdit = tmp;
		final String caption = toEdit.getCaption();
		
		//stuff
		Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(primaryStage);
        VBox dialogVbox = new VBox(20);
          
          TextField captionField = new TextField(caption);
          
          Button ok = new Button("CONFIRM");
          Button cancel = new Button("CANCEL");

          dialogVbox.getChildren().addAll(captionField, ok, cancel);
          dialogVbox.setPadding(new Insets(10, 50, 50, 50));
          /////If User clicks on OK we go here////////////////////
          ok.setOnAction(new EventHandler<ActionEvent>() {
       	   
  	        @Override
  	    	public void handle(ActionEvent e){		    	       	        		
  	        	toEdit.setCaption(captionField.getText());
				   dialog.close();
  	        	
  	        }});
          
          /////If User hits CANCEL button, just exit window///////
          cancel.setOnAction(new EventHandler<ActionEvent>() {
       	        @Override
       	    	public void handle(ActionEvent e){
    				   dialog.close();
       	        	
       	        }});
          
          Scene dialogScene = new Scene(dialogVbox, 300, 150);
          dialog.setScene(dialogScene);
          dialog.showAndWait();
		
          
          
		photopane.getChildren().clear();
	    for (int i = 0; i < album.getPhotos().size(); i++) {
	    	VBox vbox = createVBox(album.getPhotos().get(i));
	       photopane.getChildren().add(vbox);
	    }
	    selectedTile = null;
		
		}//
		
		
	}
	
	/**
	 * Allows user to MOVE a photo to another album, which removes the photo in the original album. Also allows
	 * user the option to COPY a photo, which does not remove the photo in the original album. Launches a window with a listView
	 * containing all the user's albums, which the user can select from. Cannot copy/move more into ONE album more than once, but 
	 * can move/copy to multiple different albums.
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void movePhoto(ActionEvent event) throws IOException
	{
		ArrayList<Photo> photos = album.getPhotos();
		if (selectedTile != null){
			String uri = ((Label) selectedTile.getChildren().get(3)).getText();
			for (int i = 0; i < photos.size(); i++){
				if (uri.equals(photos.get(i).getURI())){
					AlbumViewController.photoEdit = photos.get(i);
					break;
				}
			}
			
			
			Stage stage = new Stage();
			FXMLLoader loader = new FXMLLoader();
		    loader.setLocation(getClass().getResource("/view/MovePhotoView.fxml"));
			AnchorPane root = (AnchorPane)loader.load();
			Scene scene = new Scene(root);
			MovePhotoController controller = loader.getController();
			controller.start(stage);
			stage.setScene(scene);
			stage.setResizable(false);
			stage.setTitle("Move/Copy Photo");
			stage.showAndWait();
		}

		selectedTile = null;
		photopane.getChildren().clear();
	    for (int i = 0; i < photos.size(); i++) {
           VBox vbox = createVBox(photos.get(i));
	       photopane.getChildren().add(vbox);
	    }
	}
	
	/**
	 * Allows user to edit the tags of a selected photo, presented in a new window with a listView, and a textField + comboBox.
	 * Cannot add two tags of the same type + value (e.g. cannot have "location + earth" more than once).
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void editTags(ActionEvent event) throws IOException
	{
		//stuff
		
		ArrayList<Photo> photos = album.getPhotos();
		if (selectedTile != null){
			String uri = ((Label) selectedTile.getChildren().get(3)).getText();
			for (int i = 0; i < photos.size(); i++){
				if (uri.equals(photos.get(i).getURI())){
					AlbumViewController.photoEdit = photos.get(i);
					break;
				}
			}
			
			
			Stage stage = new Stage();
			//PhotoAlbum.saveUsers();
			FXMLLoader loader = new FXMLLoader();
		    loader.setLocation(getClass().getResource("/view/EditTagsView.fxml"));
			AnchorPane root = (AnchorPane)loader.load();
			Scene scene = new Scene(root);
			EditTagsController controller = loader.getController();
			controller.start(stage);
			stage.setScene(scene);
			stage.setResizable(false);
			stage.setTitle("Edit Tags");
			stage.showAndWait();
		}

		selectedTile = null;
		photopane.getChildren().clear();
	    for (int i = 0; i < photos.size(); i++) {
           VBox vbox = createVBox(photos.get(i));
	       photopane.getChildren().add(vbox);
	    }
		
		
	}
	
	
	/**
	 * Handles when the user selects File->Logout. Brings program back to the LoginScreen, where user can enter in new credentials.
	 * Saves data.
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void handleLogout(ActionEvent event) throws IOException
	{
		PhotoAlbum.saveData();
		FXMLLoader loader = new FXMLLoader();
	    loader.setLocation(getClass().getResource("/view/LoginScreen.fxml"));
		AnchorPane root = (AnchorPane)loader.load();
		Scene scene = new Scene(root);
		LoginScreenController controller = loader.getController();
		controller.start(primaryStage);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	/**
	 * Handles when user selects File->Exit, saves data and closes the program.
	 * @param event
	 */
	@FXML
	private void handleExit(ActionEvent event)
	{
		
		PhotoAlbum.saveData();
		primaryStage.close();
	}
	
	/**
	 * Allows user to delete a photo from the current album. Asks for confirmation first, then removes.
	 * @param event
	 */
	@FXML 
	private void handleDelete(ActionEvent event)
	{
		Alert alert = new Alert(AlertType.CONFIRMATION);
	   	 alert.setTitle("Delete Confirm");
	   	 alert.setHeaderText("Would you like to delete this photo?");
	   	 alert.setContentText("Please click to confirm.");

	   	 Optional<ButtonType> result = alert.showAndWait();
	   	 
	   	 
	   	 if (result.get() == ButtonType.OK){
	   		
	   		 
			if (selectedTile != null){
				String title = ((Label) selectedTile.getChildren().get(1)).getText();
				title = title.substring(9);
				System.out.println(title);
				for (int i = 0; i < album.getPhotos().size(); i++){
					System.out.println(album.getPhotos().get(i).getCaption());
					if (title.equals(album.getPhotos().get(i).getCaption())){ //change to caption or w/e later
						System.out.println("found");
						album.getPhotos().remove(i);
						break;
					}
				}
			}
			photopane.getChildren().clear();
		    for (int i = 0; i < album.getPhotos().size(); i++) {
		    	VBox vbox = createVBox(album.getPhotos().get(i));
		       photopane.getChildren().add(vbox);
		    }
		    selectedTile = null;
				
	   		 
	   	 } else {
	   	     /////If cancel, don't do anything just close////
	   	 }
	}
	
	/**
	 * Creates the vBoxes that populate the tilePane of this window, vBox contains photo thumbnail, caption label, and
	 * date label.
	 * @param photo	the given photo this vbox will represent
	 * @return a fully constructed vbox
	 */
	public VBox createVBox(Photo photo){
		Image image = new Image(photo.getURI());
		ImageView imageView = new ImageView(image);
		imageView.setPreserveRatio(true);
		imageView.setSmooth(true);
		imageView.setFitWidth(135);
		Label uri = new Label(photo.getURI());
		uri.managedProperty().bind(uri.visibleProperty());
		uri.setVisible(false);
	    Label title = new Label("Caption: " + photo.getCaption());
	    Label date = new Label("Date: " + new SimpleDateFormat("MM.dd.YYYY").format(photo.getDate()));
	    
	    VBox vbox = new VBox(imageView, title, date, uri);
	    
	    date.setAlignment(Pos.BOTTOM_LEFT);

	    vbox.setOnMousePressed(new EventHandler<MouseEvent>(){
	    	

			@Override
	    	public void handle(MouseEvent e){
	    		ObservableList<Node> nodes = photopane.getChildren();
	    		for (int i = 0; i < nodes.size(); i++){
	    			nodes.get(i).setStyle("-fx-background-color: white;");
	    		}
	    		vbox.setStyle("-fx-background-color: #ccffff;");
	    		selectedTile = vbox;
	    	}
	    });

	    //double clicking, go to photoviewer
	    vbox.setOnMouseClicked(new EventHandler<MouseEvent>() {
	        @Override
	        public void handle(MouseEvent m) {
	            if(m.getButton().equals(MouseButton.PRIMARY)){
	                if(m.getClickCount() == 2){
	                	try{
	                		ArrayList<Photo> photos = album.getPhotos();
	            			if (selectedTile != null){
	            				String uri = ((Label) selectedTile.getChildren().get(3)).getText();
	            				for (int i = 0; i < photos.size(); i++){
	            					if (uri.equals(photos.get(i).getURI())){
	            						AlbumViewController.photoEdit = photos.get(i);
	            						break;
	            					}
	            				}
	            			}
	            			Stage stage = new Stage();
	                		FXMLLoader loader = new FXMLLoader();
		        		    loader.setLocation(getClass().getResource("/view/PhotoView.fxml"));
		        			AnchorPane root = (AnchorPane)loader.load();
		        			Scene scene = new Scene(root);
		        			PhotoViewController controller = loader.getController();
		        			controller.start(stage);
		        			stage.setScene(scene);
		        			stage.setResizable(false);
		        			stage.setTitle("Photo Viewer");
		        			stage.showAndWait();
	                	}catch (Exception e){
	                		e.printStackTrace();
	                	}

	                }
	            }
	        }
	    });
	    return vbox;
	}
	
	
	
}
