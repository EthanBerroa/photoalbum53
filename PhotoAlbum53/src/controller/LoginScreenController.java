package controller;

import java.io.IOException;

import application.PhotoAlbum;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import controller.AdminScreenController;

/**
 * Controller for the loginScreen window. The window contains a textfield for username and password, although it only
 * checks for a valid username.
 * @author Ethan
 *
 */
public class LoginScreenController {
	
	@FXML
	private Button login;
	@FXML
	private Button quit;
	@FXML
	private TextField username;
	@FXML
	private TextField passWord;

	private Stage primaryStage;

	@FXML
	private void initialize(){
		
	}
	
	/**
	 * Either brings user to their list of albums, or enters into the admin screen depending on whether username="admin" or not.
	 * @param event
	 * @throws IOException
	 */
	@FXML
	private void clickLogin(ActionEvent event) throws IOException
	{
		
		String name = username.getText();
		
		FXMLLoader loader = new FXMLLoader();
		
		if (name.equals("admin")){
			
		    loader.setLocation(getClass().getResource("/view/AdminScreen.fxml"));
			AnchorPane root = (AnchorPane)loader.load();
			Scene scene = new Scene(root);
			
			AdminScreenController controller = loader.getController();
			controller.start(primaryStage);
			primaryStage.setScene(scene);
			primaryStage.show();
		}
		else{
			
			for(int i = 0; i < PhotoAlbum.users.size(); i++)
			{
				
				if(name.equals(PhotoAlbum.users.get(i).name))
				{
					PhotoAlbum.currentuser = PhotoAlbum.users.get(i);
					
					loader.setLocation(getClass().getResource("/view/UserScreen.fxml"));
					AnchorPane root = (AnchorPane)loader.load();
					Scene scene = new Scene(root);
					
					UserScreenController controller = loader.getController();
					controller.start(primaryStage);
					primaryStage.setScene(scene);
					primaryStage.show();
					break;
				}
			}
		}
		
		
	}
	
	/**
	 * Saves user data and exits the program.
	 * @param event
	 */
	@FXML //exits program
	private void clickQuit(ActionEvent event){
		try{
			PhotoAlbum.saveData();
    		primaryStage.close();
    	}catch (Exception e){
    		e.printStackTrace();
    	}
	}
	
	/**
	 * Start method, sets local field primaryStage to given parameter stage.
	 * @param primaryStage
	 */
	public void start(Stage primaryStage){
		this.primaryStage = primaryStage;
	}
}
