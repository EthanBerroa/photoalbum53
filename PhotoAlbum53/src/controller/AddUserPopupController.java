package controller;

import java.io.IOException;

import application.PhotoAlbum;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import controller.AdminScreenController;
import model.User;

/**
 * A small controller to handle the popup window that occurs when an admin adds a new user to the system.
 * Asks for username and password, although the password does not matter in this implementation.
 * @author Ethan Berroa
 * @author Milan Patel
 *
 */
public class AddUserPopupController {

	private Stage stage;
	
	@FXML
	private TextField username;
	
	@FXML
	private TextField password;
	
	/**
	 * Empty initialize, does not need to do anything as the popup window is simply two textfields and two labels.
	 */
	@FXML
	private void initiliaze()
	{
		
	}
	
	/**
	 * Method for when admin clicks confirm. Adds a new user object to the system, with the specified name and password. 
	 * Username cannot be empty, nor a duplicate. Password does not matter.
	 */
	@FXML
	private void handleConfirm()
	{
		String user = username.getText();
		String pass = password.getText();
		User tmp = new User(user,pass);
		if(!user.equals("") && !PhotoAlbum.users.contains(tmp))
			PhotoAlbum.users.add(tmp);
		stage.close();
	}
	
	/**
	 * Admin clicks cancel, closes the window and does not record any data in the textFields.
	 */
	@FXML
	private void handleCancel()
	{
		stage.close();
	}

	/**
	 * Start method, sets local field "stage" to given parameter stage.
	 * @param stage
	 */
	public void start(Stage stage) {
		// TODO Auto-generated method stub
		this.stage = stage;
	}
	
}
