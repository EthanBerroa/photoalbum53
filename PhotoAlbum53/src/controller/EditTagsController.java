package controller;

import java.util.ArrayList;

import application.PhotoAlbum;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Photo;
import model.Tag;

/**
 * A small controller class meant to handle the EditTagsView, the window that pops up when a user decides to edit a photo's tags.
 * @author Ethan Berroa
 * @author Milan Patel
 *
 */
public class EditTagsController {
	
	
	private Stage primaryStage;

	private Photo photo;
	private ArrayList<Tag> tags;
	
	@FXML
	private TextField tagField;
	
	@FXML
	private ComboBox<String> typeMenu;

	private ObservableList<Tag> tagsObservable;

	@FXML
	private ListView tagList;
	
	/**
	 * Populates listview with photo's current tags, and initializes comboBox typeMenu to contain the three tag types:
	 * Location, Name, and Other
	 */
	@FXML
	private void initialize(){
		
		
		this.photo = AlbumViewController.photoEdit;
		this.tags = photo.tags;
		typeMenu.getItems().addAll("Location", "Name", "Other");
		tagsObservable = FXCollections.observableArrayList();
		tagsObservable.addAll(tags);
		tagList.setItems(tagsObservable);
        tagList.getSelectionModel().select(0);
	   
	}

	/**
	 * Start method, sets local field stage to given parameter stage.
	 * @param primaryStage
	 */
	public void start(Stage primaryStage){
		this.primaryStage = primaryStage;
	}
	
	

	/**
	 * Adds the entered-in tags to the photo, disallows duplicates (duplicate meaning same type+value).
	 * @param event
	 */
	@FXML
	private void handleAddTag(ActionEvent event)
	{
		
		String[] newTags = tagField.getText().split(" ");
		String type = typeMenu.getValue();
		
		if(type == null)
			return;
		
		for(int i = 0; i < newTags.length; i++)
		{
			Tag tmp = new Tag(type, newTags[i]);
			if(!AlbumViewController.photoEdit.containsTag(tmp))
			{
				AlbumViewController.photoEdit.addTag(new Tag(type, newTags[i]));
			}
		}
		
		tagsObservable.clear();
		tagsObservable.addAll(tags);
		tagList.setItems(tagsObservable);
        tagList.getSelectionModel().select(0);
		
	}
	
	/**
	 * Removes a selected tag from the listView from the Photo itself.
	 * @param event
	 */
	@FXML
	private void handleDeleteTag(ActionEvent event)
	{
		if(tags.size()>0)
		{
		 int removeIndex = tagList.getSelectionModel().getSelectedIndex();
		 
		 tags.remove(removeIndex);
		 
		 tagsObservable.clear();
		tagsObservable.addAll(tags);
		tagList.setItems(tagsObservable);
	    tagList.getSelectionModel().select(0);
		}
	}
	
	/**
	 * Closes the editTag window.
	 * @param event
	 */
	@FXML
	private void closeTagEdit(ActionEvent event)
	{
		primaryStage.close();
	}
	
}
