package controller;

import java.text.SimpleDateFormat;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Photo;

/**
 * Controller for photoView, shows the enlarged photo image along with caption, date, and tags. Also allows for viewing next/previous 
 * photos within the album.
 * @author Ethan Berroa
 * @author Milan Patel
 *
 */
public class PhotoViewController {

	@FXML
	private ImageView display;
	
	@FXML
	private Label caption;
	
	@FXML
	private Label date;
	
	@FXML
	private Label tags;

	@FXML
	private VBox vbox;
	
	@FXML 
	private Pane pane;
	
	private Stage primaryStage;
	
	private Photo photo;
	
	private int index;
	
	
	/**
	 * Initializes imageView to the selected photo.
	 */
	@FXML
	private void initialize(){
		index=-1;
		photo = AlbumViewController.photoEdit;
		displayPhoto();
	}

	/**
	 * Start method, initializes primaryStage field.
	 * @param primaryStage
	 */
	public void start(Stage primaryStage){
		this.primaryStage = primaryStage;
	}
	
	
	/**
	 * Handles when the user clicks the "next" button, shifts the photoView over to the next photo in the album.
	 * @param event
	 */
	@FXML
	private void handleNext(ActionEvent event)
	{
		if(index == -1)
			index = AlbumViewController.album.getPhotos().indexOf(photo);
		index++;
		if(index < AlbumViewController.album.getPhotos().size())
		{
			photo = AlbumViewController.album.getPhotos().get(index);
			displayPhoto();
		}
	
	}
	
	/**
	 * Handles when the user clicks the previous button, shifts the photoView over to the previous photo in the album.
	 * @param event
	 */
	@FXML
	private void handlePrevious(ActionEvent event)
	{
		if(index == -1)
			index = AlbumViewController.album.getPhotos().indexOf(photo);
		index--;
		if(index > -1)
		{
			photo = AlbumViewController.album.getPhotos().get(index);
			displayPhoto();
		}
	}
	
	/**
	 *Closes the photoView window, going back to the albumView.
	 * @param event
	 */
	@FXML
	private void handleClose(ActionEvent event)
	{
		primaryStage.close();
	}
	
	
	/**
	 * Retrieves caption, date, tag, and actual photo image to fill the imageView and labels with.
	 */
	private void displayPhoto()
	{
		
		Image image = new Image(photo.getURI());
		
		display.setImage(image);
		display.setPreserveRatio(true);
		display.setSmooth(true);
		display.setFitWidth(500);
		caption.setText(photo.getCaption());
		date.setText(new SimpleDateFormat("yyyy-MM-dd").format(photo.getDate()));
		String tmp = "";
		for(int i = 0; i < photo.tags.size(); i++)
		{
			if(i == photo.tags.size()-1)
			{
				tmp += photo.tags.get(i).getValue();
			}
			else
			{
				tmp += photo.tags.get(i).getValue() + ", ";
			}
		}
		tags.setText(tmp);
	}
	
	
	
}
